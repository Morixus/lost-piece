# Lost Piece

First "bigger" project we're making (4 people). We want to make
mix of platformer and metroidvania in dark fantasy setting. Main character has
to find antidote, because water in her village was poisoned. We're planning to port it
to Mobiles. The game is still WiP.

Kacper Lewandowicz:
*  Character movement
*  Double jump
*  Patrolling enemy

Jakub Czarnecki:
* Health System
* Setting camera with cinemachine
* Collecting items and viewing it in UI
* Prototype shooting system

# Code

        if (IsGrounded())
        {
            _canDoubleJump = true;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            if (IsGrounded())
            {
                Jump();
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    if (_canDoubleJump)
                    {
                        Jump();
                        _canDoubleJump = false;
                    }
                }
            }
        }

**Checking if player is on the ground and based on that allowing him to jump**

    private void Awake()
    {
        _button.GetComponent<Button>();
        _button.onClick.AddListener(Shoot);  
    }
 
    void Shoot()
    {
        StartCoroutine(CoolDown(_coolDownTime));
    }
    IEnumerator CoolDown(float second)
    {
        _button.interactable = false;
        Instantiate(_bulletPrefab, _firePosition.position, _firePosition.rotation);
        yield return new WaitForSeconds(second);
        _button.interactable = true;
    }

**Shooting under the button and cooldown**

    private void Start()
    {
        _disabled = true;
        _objectToDisable.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
  
        PlayerController player = other.GetComponent<PlayerController>();
        if(player != null)
        {
            _disabled = false;
            PlayerController._activeDoubleJump = true;
            Destroy(gameObject);
            ActiveUIMedalion();
        }
    }
    private void ActiveUIMedalion()
    {
        if (_disabled == false)
        {
            _objectToDisable.SetActive(true);
        }
   
    }
    
**Collecting a piece of amulet, activating skills and UI**
    
        if(_health <= 0)
        {
            Destroy(gameObject);
            int randomChange = Random.Range(0, 2);
            
            if (randomChange == 0)
            {
                Instantiate(_loot, transform.position, Quaternion.identity);
            }
        }
        
**Random loot after enemy dies**