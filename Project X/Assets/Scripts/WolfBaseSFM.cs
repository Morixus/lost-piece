﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfBaseSFM : StateMachineBehaviour
{
    public GameObject wolf;
    public GameObject player;
    public Animator anim;
    public float speed;
    public Transform myTrans;
    public Rigidbody2D rb;
    public float myWidth;
    public float detectLength;
    public LayerMask enemyMask;
    public LayerMask playerMask;
    public GameObject manager;
    public Manager test;
    public AudioSource wolfIdleSound;
    public AudioSource wolfAttackSound;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        wolf = animator.gameObject;
        player = wolf.GetComponent<WolfAI>().GetPlayer();
        myWidth = animator.gameObject.GetComponent<SpriteRenderer>().bounds.extents.x;
        myTrans = animator.gameObject.transform;
        rb = animator.gameObject.GetComponent<Rigidbody2D>();
        anim = animator.GetComponent<Animator>();

        manager = wolf.GetComponent<WolfAI>().GetTest();
        test = manager.GetComponent<Manager>();
        speed = test.speed;
        wolfIdleSound = test.wolfIdleSound;
        wolfAttackSound = test.wolfAttackSound;
    }
}