﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class WolfAI : MonoBehaviour
{
    [SerializeField] private int _health = 100;
    [SerializeField] private int _damage = 1;
    [SerializeField] private GameObject _loot;
    [SerializeField] private GameObject _attackPrefab;
    [SerializeField] private GameObject _attackSpawn;
    [SerializeField] private GameObject test;
    private int _randomMin = 0;
    private int _randomMax = 2;
    private float _slowEffect = 2f;
    public GameObject player;
    private Manager tt;

    public GameObject GetPlayer()
    {
        return player;
    }
    
    public GameObject GetTest()
    {
        return test;
    }

    private void Awake()
    {
        tt = test.GetComponent<Manager>();
    }

    private void Update()
    {
        Debug.Log("TEST");
        Debug.Log(tt.speed);
    }

    public void Attack()
    {
        Instantiate(_attackPrefab, _attackSpawn.transform.position, Quaternion.identity);
    }

    public void TakeDamage(int damage)
    {
        _health -= damage;

        if (_health <= 0)
        {
            Destroy(gameObject);
            int randomChange = Random.Range(_randomMin, _randomMax);

            if (randomChange == 0)
            {
                Instantiate(_loot, transform.position, Quaternion.identity);
            }
        }
    }

    public void SlowMove(float slowTime)
    {
        StartCoroutine(SlowEnemy(slowTime));
    }

    IEnumerator SlowEnemy(float slowTime)
    {
        tt.speed /= _slowEffect;
        yield return new WaitForSeconds(slowTime);
        tt.speed *= _slowEffect;

    }
}