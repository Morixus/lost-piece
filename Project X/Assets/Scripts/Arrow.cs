﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : ArrowControler
{
    private void OnTriggerEnter2D(Collider2D hit)
    {
        _wolfAI = hit.GetComponent<WolfAI>();
        if (_wolfAI != null)
        {
            _wolfAI.TakeDamage(_damage);
            Destroy(gameObject);
        }
        
    }
}