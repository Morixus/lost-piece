﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notes : NotesMenager
{
    private void OnTriggerEnter2D(Collider2D collect)
    {
        _player = collect.GetComponent<PlayerController>();
        if(_player != null)
        {
            _readThis.gameObject.SetActive(true);
            _readThis.onClick.AddListener(Read); 
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        _readThis.gameObject.SetActive(false);
    }
}