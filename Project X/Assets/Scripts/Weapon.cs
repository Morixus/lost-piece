﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Transform _firePosition;
    [SerializeField] private GameObject _arrowPrefab;
    [SerializeField] private GameObject _slowArrowPrefab;
    [SerializeField] private Button _buttonShot;
    [SerializeField] private Button _buttonTripleShot;
    [SerializeField] private Button _buttonSlowArrow;
    [SerializeField] private float _coolDownShotTime;
    [SerializeField] private float _coolDownTripleShotTime;
    [SerializeField] private float _coolDownSlowArrow;
    [SerializeField] private AudioSource _attackSound;
    private float _timeBetweeBullets = 0.1f;
    [SerializeField] private float _break = 0.5f;
    public static bool activeTropleShoot;
    public static bool activeSlowArrow;
    private int _tripleShoot = 3;
    private bool _tripleShootIsUsed;
    private bool _slowShootIsUsed;
    private bool _shootIsUsed;

    private void Awake()
    {
        _buttonShot.GetComponent<Button>();
        _buttonTripleShot.GetComponent<Button>();
        _buttonSlowArrow.GetComponent<Button>();
        _buttonShot.onClick.AddListener(Shoot);
        _buttonTripleShot.onClick.AddListener(TripleShot);
        _buttonSlowArrow.onClick.AddListener(SlowShoot);
        _buttonTripleShot.gameObject.SetActive(false);
        _buttonSlowArrow.gameObject.SetActive(false);
        activeTropleShoot = false;
        activeSlowArrow = false;
    }
    private void LateUpdate()
    {
        Active();
    }
    public  void Active()
    {
        if (activeTropleShoot == true)
        {
            _buttonTripleShot.gameObject.SetActive(true);
        }
        if (activeSlowArrow == true)
        {
            _buttonSlowArrow.gameObject.SetActive(true);
        }
    }
    void Shoot()
    {
        StartCoroutine(CoolDown(_coolDownShotTime));
    }
    void TripleShot()
    {
        StartCoroutine(CoolDownTripleShot(_coolDownTripleShotTime));
    }
    void SlowShoot()
    {
       StartCoroutine(CoolDownSlowShot(_coolDownSlowArrow));
    }
    IEnumerator CoolDown(float second)
    {
        _shootIsUsed = true;
        _buttonShot.interactable = false;
        Instantiate(_arrowPrefab, _firePosition.position, _firePosition.rotation);
        _attackSound.Play();
        yield return new WaitForSeconds(second);
        _shootIsUsed = false;
        _buttonShot.interactable = true;
    }
   IEnumerator CoolDownTripleShot(float second)
    {
        _tripleShootIsUsed = true;
        _buttonTripleShot.interactable = false;
        if (_slowShootIsUsed == false)
        {
            _buttonSlowArrow.interactable = false;
        }
        if (_shootIsUsed == false)
        {
            _buttonShot.interactable = false;
        }
        for (int i = 0; i < _tripleShoot; i++)
            {
                Instantiate(_arrowPrefab, _firePosition.position, _firePosition.rotation);
                _attackSound.Play();
                yield return new WaitForSeconds(_timeBetweeBullets);
            }
         yield return new WaitForSeconds(_break);
        if (_shootIsUsed == false)
        {
            _buttonShot.interactable = true;
        }
        if (_slowShootIsUsed == false)
        { 
            _buttonSlowArrow.interactable = true;
        }
         yield return new WaitForSeconds(second);
        _tripleShootIsUsed = false;
        _buttonTripleShot.interactable = true;
    }
    IEnumerator CoolDownSlowShot(float second)
    {
        _slowShootIsUsed = true;
        _buttonSlowArrow.interactable = false;
        if (_tripleShootIsUsed == false)
        {
            _buttonTripleShot.interactable = false;
        }
        if (_shootIsUsed == false)
        {
            _buttonShot.interactable = false;
        }
        Instantiate(_slowArrowPrefab, _firePosition.position, _firePosition.rotation);
        _attackSound.Play();
        yield return new WaitForSeconds(_break);
        if (_shootIsUsed == false)
        {
            _buttonShot.interactable = true;
        }
        if (_tripleShootIsUsed == false)
        {
            _buttonTripleShot.interactable = true;
        }
        yield return new WaitForSeconds(second);
        _slowShootIsUsed = false;
        _buttonSlowArrow.interactable = true;    
    }
}
