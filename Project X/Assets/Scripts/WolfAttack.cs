﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class WolfAttack : WolfBaseSFM
{
    [SerializeField] private float cooldown;
    private WolfAI _wolfAI;
    private float timer;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        _wolfAI = animator.GetComponent<WolfAI>();
        speed = 0f;
        timer = cooldown;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Vector2 lineCastPos = myTrans.position + myTrans.right * myWidth * 0.5f;
        Debug.DrawLine(lineCastPos, lineCastPos + myTrans.right.toVector2() * detectLength, Color.red);
        bool isDetecting = Physics2D.Linecast(lineCastPos, lineCastPos + myTrans.right.toVector2() * detectLength, playerMask);

        timer -= Time.deltaTime;
        Debug.Log(timer);
        if (timer <= 0f)
        {
            _wolfAI.Attack();
            wolfAttackSound.Play();
            timer = cooldown;
        }

        if (!isDetecting)
        {
            animator.SetBool("isDetec", false);
        }
    }
}