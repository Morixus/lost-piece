﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfChase : StateMachineBehaviour
{
    private Transform _myTrans;
    [SerializeField] private float _detectRayLength;
    private Vector3 _dirRight = new Vector3(0f, 0f, 0f);

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        RaycastHit2D playerInfoRight = Physics2D.Raycast(_myTrans.position, _myTrans.right.toVector2(), _detectRayLength);

        Debug.DrawRay(playerInfoRight.transform.position, _myTrans.right * _detectRayLength, Color.red);

        if (playerInfoRight.collider != null)
        {
            PlayerController player = playerInfoRight.collider.GetComponent<PlayerController>();
            if (player != null)
            {
                _myTrans.eulerAngles = _dirRight;
            }
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
