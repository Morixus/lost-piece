﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceOfAmuletTwo : Amulet
{
    private void OnTriggerEnter2D(Collider2D collect)
    {
        _player = collect.GetComponent<PlayerController>();
        if (_player != null)
        {
            _disabled = false;
            Weapon.activeTropleShoot = true;
            Destroy(gameObject);
            ActiveUIMedalion();
        }
    }

}
