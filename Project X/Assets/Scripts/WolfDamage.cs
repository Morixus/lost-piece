﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfDamage : MonoBehaviour
{
    [SerializeField] private int _damage = 1;
    [SerializeField] private AudioSource _playerHitSound;
    private void OnTriggerEnter2D(Collider2D hit)
    {
        PlayerController player = hit.GetComponent<PlayerController>();
        if (player != null)
        {
            Health.health -= _damage;
            _playerHitSound.Play();
        }
        Destroy(this.gameObject);
    }
}
