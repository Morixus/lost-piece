﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{

    [SerializeField] private Button _pauseButton;
    [SerializeField] private Button _returnButton;
    [SerializeField] private GameObject _pauzaMenu;
    [SerializeField] private GameObject _loadingScreen;
    [SerializeField] private Slider _slider;
    private float _progress;
    private float _time = 0.9f;


    private void Awake()
    {
        _pauseButton.GetComponent<Button>();
        _returnButton.GetComponent<Button>();
        _pauseButton.onClick.AddListener(Pausa);
        _returnButton.onClick.AddListener(Return);
    }


    private void Pausa()
    {
        _pauseButton.interactable = false;   
        Time.timeScale = 0f;
        _pauzaMenu.SetActive(true);
    }
    private void Return()
    {
        _pauzaMenu.SetActive(false);
        Time.timeScale = 1f;
        _pauseButton.interactable = true;

    }
    private void BackToMainMenu(int sceneIndex)
    {
        Time.timeScale = 1f;
        StartCoroutine(Loading(sceneIndex));
    }
    IEnumerator Loading(int sceneIdex)
    {
        _loadingScreen.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIdex);
        while (!operation.isDone)
        {
            _progress = Mathf.Clamp01(operation.progress / _time);
            _slider.value = _progress;
            yield return null;
        }
    }
     private void Quit()
    {
        Application.Quit();
    }

}


