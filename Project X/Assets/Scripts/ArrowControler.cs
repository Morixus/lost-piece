﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowControler : MonoBehaviour
{
    [SerializeField] protected float _speed = 10f;
    private Rigidbody2D _rb;
    [SerializeField] protected int _damage = 20;
    [SerializeField] protected float _delatedBullet;
    protected WolfAI _wolfAI;
    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rb.velocity = transform.right * _speed;
        Destroy(gameObject, _delatedBullet);
    }
}
