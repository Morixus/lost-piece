﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceOfAmuletOne : Amulet
{
    private void OnTriggerEnter2D(Collider2D collect)
    {
        _player = collect.GetComponent<PlayerController>();
        if (_player != null)
        {
            _disabled = false;
            PlayerController._activeDoubleJump = true;
            Destroy(gameObject);
            ActiveUIMedalion();
        }
    }
}