﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Amulet : MonoBehaviour
{
    [SerializeField] protected GameObject _objectToDisable;
    protected bool _disabled;
    protected PlayerController _player;

    private void Start()
    {
        _disabled = true;
        _objectToDisable.SetActive(false);

    }
    protected void ActiveUIMedalion()
    {
        if (_disabled == false)
        {
            _objectToDisable.SetActive(true);
        }
    }
}

    
