﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public float speed = 5;
    public AudioSource wolfIdleSound;
    public AudioSource wolfAttackSound;
}