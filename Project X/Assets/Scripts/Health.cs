﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public static int health;
    public static int numOfHearts;

    [SerializeField] private Image[] _hearts;
    [SerializeField] private Sprite _fullHeart;
    [SerializeField] private Sprite _emptyHeart;
    [SerializeField] private GameObject _gameOver;
    private void Start()
    {
        StartHealth();
    }
    private void FixedUpdate()
    {
        CheckHealth();
        HealthSprite();
        Death();
    }


    void HealthSprite()
    {
        for (int i = 0; i < _hearts.Length; i++)
        {
            if (i < health)
            {
                _hearts[i].sprite = _fullHeart;
            }
            else
            {
                _hearts[i].sprite = _emptyHeart;
            }
            if (i < numOfHearts)
            {
                _hearts[i].enabled = true;
            }
            else
            {
                _hearts[i].enabled = false;
            }
        }
    }
    void CheckHealth()
    {
        if (health > numOfHearts)
        {
            health = numOfHearts;
        }
    }
    void Death()
    {
        if (health == 0)
        {
            _gameOver.gameObject.SetActive(true);
            Time.timeScale = 0;
            Debug.Log("smth");
        }
    }
    void StartHealth()
    {
        health = 3;
        numOfHearts = 3;
        _gameOver.gameObject.SetActive(false);
    }
}