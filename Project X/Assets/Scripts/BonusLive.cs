﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusLive : MonoBehaviour
{
    private int _bonusLive = 1;
    private void OnTriggerEnter2D(Collider2D other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        {
            if (player != null)
            {
                Health.numOfHearts += _bonusLive;
                Destroy(gameObject);
            }
           
        }

    }
}