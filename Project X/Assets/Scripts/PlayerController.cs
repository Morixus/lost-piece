﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;
    [SerializeField] private LayerMask _groundLayer;
    [SerializeField] private AudioSource _jumpSound;
    private Rigidbody2D _rb;
    private Collider2D _collider;
    private bool _canDoubleJump;
    public static bool _activeDoubleJump;
    private bool _lookRight;
    private float _horizontal;



    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
        _activeDoubleJump = false;
        _lookRight = true;
       
    }
    private void FixedUpdate()
    {
         
    _rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        //Sterowanie
        if (Input.GetKey(KeyCode.A))
        {
            _rb.velocity = new Vector2(-_speed, _rb.velocity.y);

        }
        else
        {
            if (Input.GetKey(KeyCode.D) )
            {
                _rb.velocity = new Vector2(+_speed, _rb.velocity.y);
               
            }
            else
            {
                //dziala gdy się nie ruszamy
                _rb.velocity = new Vector2(0, _rb.velocity.y);
                _rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
            }
        }
        // obracanie się postaci
         _horizontal = Input.GetAxisRaw("Horizontal");
        if (_horizontal > 0 && _lookRight || _horizontal < 0 && !_lookRight)
        {
            Flip();
        }
      
       
    }
    private void Update()
    {
       
        if (IsGrounded())
        {
            _canDoubleJump = true;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            if (IsGrounded())
            {
                Jump();
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Space) && _activeDoubleJump == true)
                {
                    if (_canDoubleJump)
                    {
                        Jump();
                        _canDoubleJump = false;
                    }
                }
            }
        }
    }
    private void Jump()
    {
        _rb.velocity = Vector2.up * _jumpForce;
        _jumpSound.Play();
    }
    private bool IsGrounded()
    {
        //przedłużenie boxcasta
        float extraHeight = 0.2f;
        //Boxcast pod graczem sprawdzający czy dotyka ziemi
        RaycastHit2D raycastHit = Physics2D.BoxCast(_collider.bounds.center, _collider.bounds.size, 0f, Vector2.down, extraHeight, _groundLayer);
        return raycastHit.collider != null;
    }
    private void Flip()
    {
        _lookRight = !_lookRight;
        transform.Rotate(0f, 180f, 0f);
    }
    
}
