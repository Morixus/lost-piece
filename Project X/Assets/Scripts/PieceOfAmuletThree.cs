﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceOfAmuletThree : Amulet
{
    private void OnTriggerEnter2D(Collider2D collect)
    {
        _player = collect.GetComponent<PlayerController>();
        if (_player != null)
        {
            _disabled = false;
            Weapon.activeSlowArrow = true;
            Destroy(gameObject);
            ActiveUIMedalion();
            
        }
    }
}
