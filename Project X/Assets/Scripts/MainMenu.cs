﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject _loadingScreen;
    [SerializeField] private Slider _slider;
    private float _progress;
    private float _time = 0.9f;

    private void PlayGame(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }
    private void QuitGame()
    { 
        Application.Quit();
    }

    IEnumerator LoadAsynchronously(int sceneIdex)
    {
        _loadingScreen.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIdex);
        while (!operation.isDone)
        {
            _progress = Mathf.Clamp01(operation.progress / _time);
            _slider.value = _progress;
            yield return null;
        }
    }

}
