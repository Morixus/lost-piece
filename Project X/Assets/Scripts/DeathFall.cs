﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathFall : MonoBehaviour
{
    private float _playerPosition;
    [SerializeField] private float _fallToDeath;
    private void LateUpdate()
    {
        _playerPosition = transform.position.y;
        if (_playerPosition <= _fallToDeath)
        {
            Health.health -= Health.health;

        }
    }
}