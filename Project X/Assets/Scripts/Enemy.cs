﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private int _health = 100;
    [SerializeField] private int _damage = 1;
    [SerializeField] private GameObject _loot;
    [SerializeField] private float _speed;
    [SerializeField] private float _groundRayLength;
    [SerializeField] private float _detectRayLength;
    [SerializeField] private Transform _groundDetector;
    [SerializeField] private GameObject _player;
    [SerializeField] private float _cooldown;
    [SerializeField] private GameObject _attackPrefab;
    [SerializeField] private Transform _attackSpawn;
    private Rigidbody2D _rb;
    private bool _movingRight = true;
    private Vector3 _dirRight = new Vector3(0f, 0f, 0f);
    private Vector3 _dirLeft = new Vector3(0f, -180f, 0f);
    private float _distance;
    [SerializeField] private float _attackDistance;
    private int _randomMin = 0;
    private int _randomMax = 2;
    private float _timer;
// Project X/Assets/Scripts/Enemy.cs
    private float _slowEffect = 2f;

    [SerializeField] private LayerMask _enemyMask;
    [SerializeField] private LayerMask _playerMask;
    private Transform _myTrans;
    private float _myWidth;
// Project X/Assets/Scripts/Enemy.cs

    private void Start()
    {
        _timer = _cooldown;
        _rb = GetComponent<Rigidbody2D>();
        _myTrans = this.transform;
        _myWidth = this.GetComponent<SpriteRenderer>().bounds.extents.x;
    }

    private void Update()
    {
        /*_distance = Vector2.Distance(transform.position, _player.transform.position);
        Debug.Log(_distance);
        if (_distance <= _attackDistance)
        {
            Debug.Log("Attack");
        }*/
    }

    private void FixedUpdate()
    {
        Patrol();
        Follow();
    }

    private void Patrol()
    {
        Vector2 lineCastPos = _myTrans.position + _myTrans.right * _myWidth * 0.5f;
        Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down * 2f);
        bool isGrounded = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down * 2f, _enemyMask);
        Debug.DrawLine(lineCastPos, lineCastPos + _myTrans.right.toVector2());
        bool isBlocked = Physics2D.Linecast(lineCastPos, lineCastPos + _myTrans.right.toVector2(), _enemyMask);

        if (!isGrounded || isBlocked)
        {
            Vector3 currRot = _myTrans.eulerAngles;
            currRot.y += 180;
            transform.eulerAngles = currRot;
        }

        Vector2 myVel = _rb.velocity;
        myVel.x = _myTrans.right.x * _speed;
        _rb.velocity = myVel;
    }

    private void Follow()
    {
        RaycastHit2D playerInfoRight = Physics2D.Raycast(_myTrans.position, _myTrans.right.toVector2(), _detectRayLength, _playerMask);

        Debug.DrawRay(playerInfoRight.transform.position, _myTrans.right * _detectRayLength, Color.red);

        if (playerInfoRight.collider != null)
        {
            PlayerController player = playerInfoRight.collider.GetComponent<PlayerController>();
            if (player != null)
            {
                transform.eulerAngles = _dirRight;
            }
        }
    }

    public void TakeDamage(int damage)
    {
        _health -= damage;

        if (_health <= 0)
        {
            Destroy(gameObject);
            int randomChange = Random.Range(_randomMin, _randomMax);

            if (randomChange == 0)
            {
                Instantiate(_loot, transform.position, Quaternion.identity);
            }
        }
    }
    public void SlowMove(float slowTime)
    {
        StartCoroutine(SlowEnemy(slowTime));
    }
    IEnumerator SlowEnemy(float slowTime)
    {
        _speed /= _slowEffect;
        yield return new WaitForSeconds(slowTime);
        _speed *= _slowEffect;

    }
}