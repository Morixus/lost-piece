﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class NotesMenager : MonoBehaviour
{
    [SerializeField] protected GameObject _notes;
    protected PlayerController _player;
    [SerializeField] protected Button _readThis;
    [SerializeField] protected Button _leaveThis;
    [SerializeField] protected GameObject _hideUI;
    protected float _timeStop = 0f;
    protected float _timeStart = 1f;
    [SerializeField] protected GameObject[] _notesToDisabled;
    private void Awake()
    {
        _notes.SetActive(false);
        _readThis.GetComponent<Button>();
        _leaveThis.GetComponent<Button>();
        _readThis.gameObject.SetActive(false);
        _leaveThis.gameObject.SetActive(false);
    }
    protected void Read()
    {
            _notes.SetActive(true);   
            _leaveThis.gameObject.SetActive(true);
            Time.timeScale = _timeStop;
            _leaveThis.onClick.AddListener(Leave);
            _hideUI.SetActive(false);
        for (int i = 0; i < _notesToDisabled.Length; i++)
        {
            _notesToDisabled[i].SetActive(false);
        }
        
    }
    protected void Leave()
    {
        _notes.SetActive(false);
        Time.timeScale = _timeStart;
        _hideUI.SetActive(true);
    }
}
