﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowArrow : ArrowControler
{
    [SerializeField] private float _slowTime;
    private void OnTriggerEnter2D(Collider2D hit)
    {
        _wolfAI = hit.GetComponent<WolfAI>();
        if (_wolfAI != null)
        {
            _wolfAI.TakeDamage(_damage);
            _wolfAI.SlowMove(_slowTime);
        }
        Destroy(gameObject);
    }
}