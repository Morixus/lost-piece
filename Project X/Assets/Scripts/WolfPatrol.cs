﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfPatrol : WolfBaseSFM
{
    public bool isDetected;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        /*manager = wolf.GetComponent<WolfAI>().GetTest();
        test = manager.GetComponent<Test>();*/
        wolfIdleSound.Play();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        speed = test.speed;
        Vector2 lineCastPos = myTrans.position + myTrans.right * myWidth * 0.5f;
        Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down * 2f);
        bool isGrounded = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down * 2f, enemyMask);
        Debug.DrawLine(lineCastPos, lineCastPos + myTrans.right.toVector2());
        bool isBlocked = Physics2D.Linecast(lineCastPos, lineCastPos + myTrans.right.toVector2() * detectLength, enemyMask);
        Debug.DrawLine(lineCastPos, lineCastPos + myTrans.right.toVector2() * detectLength, Color.red);
        bool isDetecting = Physics2D.Linecast(lineCastPos, lineCastPos + myTrans.right.toVector2() * detectLength, playerMask);

        if (isDetecting)
        {
            isDetected = true;
            animator.SetBool("isDetec", true);
        }
        if (!isGrounded || isBlocked)
        {
            Vector3 currRot = myTrans.eulerAngles;
            currRot.y += 180;
            myTrans.eulerAngles = currRot;
        }

        Vector2 myVel = rb.velocity;
        myVel.x = myTrans.right.x * speed;
        rb.velocity = myVel;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
            wolfIdleSound.Stop();
    }
}
